package handlers

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"net/url"
	"os"
	"strings"
)

func ServePage(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/plain")
	b, err := io.ReadAll(r.Body)
	if err != nil {
		log.Fatalln(err)
	}
	relPath, _ := url.QueryUnescape(string(b))
	//Serving default page at the root URL
	if relPath == "/" {
		relPath = cfg.DefaultPage
	}
	locale := r.Header.Get("locale")
	//Add language string to the file search path
	path := cfg.ContentPath + relPath + "." + locale + ".md"

	buf, err := os.ReadFile(path)
	if err != nil {
		//Tha language string is remove if the file is not found
		path = cfg.ContentPath + relPath + ".md"
		buf, err = os.ReadFile(path)
		if err != nil {
			log.Println(path + " not found")
			w.WriteHeader(http.StatusNotFound)
			return
		}
	}
	s := string(buf)
	lines := strings.Split(strings.ReplaceAll(s, "\r", ""), "\n")
	for i, line := range lines {
		if strings.Contains(line, "![") {
			//Replacing links to images
			lines[i] = strings.ReplaceAll(line, "(./", "("+cfg.BackEndUrl+"/file"+substringBeforeLast(relPath, "/")+"/")
		} else if strings.Contains(line, "](") && strings.Contains(line, ".md)") {
			//Replaxing referencies to other pages
			lines[i] = strings.ReplaceAll(line, ".md)", ")")
		}
	}
	s = strings.Join(lines, "\n")

	w.WriteHeader(http.StatusOK)
	fmt.Fprint(w, s)
}

func ServeFile(w http.ResponseWriter, r *http.Request) {
	path := r.PathValue("path")
	fullPath := cfg.ContentPath + "/" + path
	_, err := os.Stat(fullPath)
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		return
	}
	http.ServeFile(w, r, fullPath)
}

func substringBeforeLast(str string, splitter string) string {
	lastIndex := strings.LastIndex(str, splitter)
	if lastIndex == -1 {
		return str
	}
	return str[:lastIndex]
}
