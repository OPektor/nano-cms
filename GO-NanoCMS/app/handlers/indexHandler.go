package handlers

import (
	"encoding/json"
	"errors"
	"log"
	"nanoCMS/app/contracts"
	"os"
	"slices"
	"strings"
	"time"
)

var cfg contracts.Config
var info = log.New(os.Stdout, "", log.LstdFlags)

func InitHandlers(config contracts.Config) {
	cfg = config
}

func UpdateIndexJson() {
	fileInfo, error := os.Stat(cfg.IndexFile)
	//Create new index.json if the latest is more than 5 minutes old
	if errors.Is(error, os.ErrNotExist) || fileInfo.ModTime().Add(time.Minute*5).Before(time.Now()) {
		info.Println("Creating index JSON")

		pages, sections, links, _ := generateItems(cfg.ContentPath, cfg.ContentPath, cfg.Locales)
		index := contracts.Index{
			Pages:       pages,
			Sections:    sections,
			WebName:     cfg.WebName,
			Links:       links,
			Locales:     cfg.Locales,
			DefaultPage: cfg.DefaultPage,
		}

		_, error = os.Stat(cfg.ContentPath + "/" + cfg.MenuImage)
		if error == nil {
			index.MenuImage = cfg.BackEndUrl + "/file/" + cfg.MenuImage
		}

		jsonBytes, err := json.Marshal(index)
		if err != nil {
			log.Fatalln("Cannot serialized index JSON")
		}
		if err := os.WriteFile(cfg.IndexFile, jsonBytes, 0666); err != nil {
			log.Fatalln(err)
		}
	}
}

func generateItems(path string, originalPath string, locales []string) (pages []contracts.Page, sections []contracts.Section, links []contracts.Link, names []contracts.LocaleName) {
	pages = []contracts.Page{}
	sections = []contracts.Section{}
	links = []contracts.Link{}
	names = []contracts.LocaleName{{
		Name: substringAfterLast(path, "/"),
	}}
	entries, err := os.ReadDir(path)
	if err != nil {
		return
	}
	for _, e := range entries {
		if e.IsDir() {
			//Recursion for subdirectories
			innerPages, innerSections, innerLinks, innerNames := generateItems(path+"/"+e.Name(), originalPath, locales)
			sections = append(sections, contracts.Section{
				Names:    innerNames,
				Path:     strings.Replace(path, originalPath, "", -1) + "/" + e.Name(),
				Pages:    innerPages,
				Sections: innerSections,
				Links:    innerLinks,
			})
		} else {
			//Indexing *.md pages
			if strings.HasSuffix(strings.ToLower(e.Name()), ".md") {
				name := strings.Replace(strings.ToLower(e.Name()), ".md", "", -1)
				locale := substringAfterLast(name, ".")
				page := contracts.Page{
					Name: name,
					Path: strings.Replace(path, originalPath, "", -1) + "/" + name,
				}
				if locale != name && locale != "" && slices.Contains(locales, locale) {
					page.Locale = locale
					page.Name = strings.Replace(page.Name, "."+locale, "", -1)
					page.Path = strings.Replace(page.Path, "."+locale, "", -1)
				}
				pages = append(pages, page)
			}
			//Indexing section.json settings files
			if strings.Contains(e.Name(), "section.json") {
				content, err := os.ReadFile(path + "/" + e.Name())
				if err == nil {
					sectionSettings := contracts.SectionSettings{}
					json.Unmarshal(content, &sectionSettings)
					links = sectionSettings.Links
					names = sectionSettings.Names
				}
			}
		}
	}
	return
}

func substringAfterLast(str string, splitter string) string {
	lastIndex := strings.LastIndex(str, splitter)
	if lastIndex == -1 {
		return str
	}
	return str[lastIndex+1:]
}
