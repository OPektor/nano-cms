package app

import (
	"fmt"
	"log"
	"nanoCMS/app/contracts"
	"nanoCMS/app/handlers"
	"net/http"
	"os"

	"github.com/rs/cors"
)

var info = log.New(os.Stdout, "", log.LstdFlags)

func Bootstrap(cfg contracts.Config) {
	info.Println("Bootstrapping the app")
	handlers.InitHandlers(cfg)

	//Update index.json at startup
	handlers.UpdateIndexJson()

	mux := http.NewServeMux()

	//Serve index.json to the frontend
	mux.HandleFunc("GET /index", func(w http.ResponseWriter, r *http.Request) {
		handlers.UpdateIndexJson()
		http.ServeFile(w, r, "./index.json")
	})

	//Http trigger to manually update the index.json file
	mux.HandleFunc("GET /index/update", func(w http.ResponseWriter, r *http.Request) {
		handlers.UpdateIndexJson()
	})

	//Endpoint for serving Markdown pages
	mux.HandleFunc("POST /page", handlers.ServePage)

	//Endpoint for serving other files
	mux.HandleFunc("GET /file/{path...}", handlers.ServeFile)

	c := cors.New(cors.Options{
		AllowedOrigins:   []string{cfg.FrontEndUrl},
		AllowCredentials: true,
		AllowedHeaders:   []string{"locale"},
	})

	handler := c.Handler(mux)

	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%s", cfg.LocalPort), handler))
}
