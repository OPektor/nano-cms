package contracts

type Index struct {
	WebName     string
	Pages       []Page
	Sections    []Section
	Links       []Link
	MenuImage   string
	Locales     []string
	DefaultPage string
}

type Page struct {
	Path   string
	Name   string
	Locale string
}

type Section struct {
	Names    []LocaleName
	Path     string
	Pages    []Page
	Sections []Section
	Links    []Link
}

type Link struct {
	Title  string
	Url    string
	Locale string
}

type SectionSettings struct {
	Names []LocaleName
	Links []Link
}

type LocaleName struct {
	Name   string
	Locale string
}
