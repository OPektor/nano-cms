package contracts

type Config struct {
	ContentPath string
	IndexFile   string
	LocalPort   string
	FrontEndUrl string
	BackEndUrl  string
	WebName     string
	DefaultPage string
	MenuImage   string
	Locales     []string
}
