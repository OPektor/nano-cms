package main

import (
	"log"
	"nanoCMS/app"
	"nanoCMS/app/contracts"
	"os"

	"github.com/spf13/viper"
)

var err error
var config contracts.Config
var info = log.New(os.Stdout, "", log.LstdFlags)

func main() {
	info.Println("NanoCMS App is starting...")
	config, err = loadConfig()
	if err != nil {
		log.Fatalln("Cannot load config file")
	} else {
		app.Bootstrap(config)
	}
}

func loadConfig() (config contracts.Config, err error) {
	viper.AddConfigPath(".")
	viper.SetConfigName("config")
	viper.SetConfigType("json")
	viper.AutomaticEnv()
	err = viper.ReadInConfig()
	if err != nil {
		return
	}

	err = viper.Unmarshal(&config)
	return
}
