import React, { useEffect, useState } from 'react';
import './App.css';
import { ApiClient } from './data/ApiClient';
import { Index } from './data/contracts/Index';
import { Navigate, Route, Routes } from 'react-router-dom';
import Page from './pages/Page';
import Header from './components/Header';

const App = () => {
  const [index, setIndex] = useState<Index | null>(null);
  const [errorMessage, setErrorMessage] = useState<string>("");
  useEffect(() => {
    ApiClient.getIndex().then(i => {
      document.title = i.WebName;
      setIndex(i);
    }).catch(() => setErrorMessage("This page cannot be loaded at this time. We apologize for this inconvenience."));
  }, []);

  return (
    <>
      {
        index ?
          <>
            <Header index={index} />
            <Routes>
              <Route path={index.DefaultPage} element={<Navigate to="/" replace={true} />} />
              <Route path="/*" element={<Page />} />
            </Routes>
          </>
          :
          errorMessage ?
            <div className="container mt-5">
              <div className="alert alert-danger text-center" role="alert">
                <h3>{errorMessage}</h3>
              </div>
            </div>
            :
            <div className="mt-5 d-flex justify-content-center">
              <div className="spinner-border" style={{ width: "3rem", height: "3rem" }} role="status">
                <span className="visually-hidden">Loading...</span>
              </div>
            </div>
      }
    </>
  );
}

export default App;
