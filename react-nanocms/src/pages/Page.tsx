import { ReactNode, useEffect, useState } from 'react';
import { ApiClient } from '../data/ApiClient';
import { useLocation } from 'react-router-dom';
import Markdown from 'react-markdown';
import LinkRenderer from '../components/LinkRenderer'
import remarkGfm from 'remark-gfm'
import "./Page.css";
import rehypeRaw from 'rehype-raw';
import { Container } from 'react-bootstrap';


const Page = () => {
  const location = useLocation();
  const [markdown, setMarkdown] = useState<string>("");
  useEffect(() => {
    ApiClient.loadPage(location.pathname).then(md => setMarkdown(md));
  }, [location]);
  return (
    <div className="d-flex flex-column">
      <Container className="content">
        <Markdown
          remarkPlugins={[remarkGfm]}
          rehypePlugins={[rehypeRaw]}
          components={{
            a: ({ node, ...props }) => <LinkRenderer href={props.href} children={props.children as ReactNode} />,
            table: ({ node, ...props }) => <table className="table table-hover table-bordered" {...props} />,
            th: ({ node, ...props }) => <th className="bg-body-secondary" {...props} />,
          }}
          children={markdown}
        />
      </Container>
      <footer className="footer mt-auto py-3 bg-body-secondary text-center">
        ©{new Date().getFullYear()}
        <a className="mx-1" href="https://gitlab.com/OPektor/nano-cms" target="_blank" rel="noreferrer">Nano CMS</a>
        - Ondřej Pektor
      </footer>
    </div>
  );
}

export default Page;