import React, { useEffect, useState } from 'react'
import { Index, Link, Page, Section } from '../data/contracts/Index';
import { LinkContainer } from 'react-router-bootstrap';
import { Container, Nav, NavDropdown, NavLink, Navbar } from 'react-bootstrap';
import "./Header.css";
import { useLocation } from 'react-router-dom';
import ThemeSwitcher from './ThemeSwitcher';
import LocaleSwitcher from './LocaleSwitcher';

interface HeaderParams {
    index: Index;
}

const Header: React.FC<HeaderParams> = ({ index }) => {
    const location = useLocation();
    const [locale, setLocale] = useState<string>("");
    const [pages, setPages] = useState<Page[]>([]);
    const [defaultPages, setDefaultPages] = useState<Page[]>([]);

    useEffect(() => {
        let filteredPages = getPages(index.Pages);
        setPages(filteredPages.filter(x => x.Path !== index.DefaultPage));
        setDefaultPages(filteredPages.filter(x => x.Path === index.DefaultPage));
    }, [index, locale]);


    const getSectionName = (section: Section): string => {
        let localeName = section.Names.find(x => x.Locale === locale);
        if (!localeName) {
            localeName = section.Names.find(x => !x.Locale);
        }
        if (!localeName) {
            localeName = section.Names[0];
        }
        return localeName.Name;
    };

    const getLinks = (links: Link[]): Link[] => {
        return links.filter(x => x.Locale === locale || !x.Locale);
    }

    const getPages = (pages: Page[]): Page[] => {
        return pages.filter(x => x.Locale === locale || !x.Locale);
    }

    return (
        <Navbar fixed="top" expand="lg" className="bg-body-secondary">
            <Container>
                <LinkContainer to="/">
                    <Navbar.Brand >
                        {index?.MenuImage ?
                            <img
                                alt="menu"
                                src={index?.MenuImage}
                                width="30"
                                height="30"
                                className="d-inline-block align-top me-3"
                            />
                            : ""}
                        {index?.WebName}
                    </Navbar.Brand>
                </LinkContainer>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="me-auto">
                        {defaultPages?.map((page, i) =>
                            <LinkContainer key={i} to="/"
                                isActive={location.pathname === "/" && page.Path === index.DefaultPage}>
                                <NavLink>{page.Name}</NavLink>
                            </LinkContainer>
                        )}
                        {pages?.map((page, i) =>
                            <LinkContainer key={i} to={page.Path}
                                isActive={location.pathname.endsWith(page.Path)}>
                                <NavLink>{page.Name}</NavLink>
                            </LinkContainer>
                        )}
                        {getLinks(index?.Links)?.map((link, i) =>
                            <NavLink key={i} href={link.Url}>{link.Title}</NavLink>
                        )}
                        {index?.Sections?.map((section, i) =>
                            <NavDropdown key={i} title={getSectionName(section)} active={location.pathname.startsWith(section.Path)}>
                                {section.Pages.map((page, j) =>
                                    <LinkContainer key={j} to={page.Path}>
                                        <NavDropdown.Item>{page.Name}</NavDropdown.Item>
                                    </LinkContainer>)}
                                {getLinks(section.Links).map((link, j) =>

                                    <NavDropdown.Item key={j} href={link.Url} target='_blank'>{link.Title}</NavDropdown.Item>
                                )}
                            </NavDropdown>
                        )}
                    </Nav>
                    <Nav className="d-flex">
                        <ThemeSwitcher />
                        <LocaleSwitcher locales={index?.Locales} updateLocale={l => setLocale(l)} />
                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    )
}

export default Header;