import React, { useEffect, useState } from 'react'
import { NavLink } from 'react-bootstrap';

const ThemeSwitcher = () => {
  const [theme, setTheme] = useState<string>('light');
  useEffect(() => {
    const savedTheme = localStorage.getItem('theme');
    if (savedTheme) {
      setTheme(savedTheme);
    } else {
      const prefersDarkScheme = window.matchMedia('(prefers-color-scheme: dark)').matches;
      setTheme(prefersDarkScheme ? 'dark' : 'light');
    }
  }, []);

  useEffect(() => {
    document.body.setAttribute('data-bs-theme', theme);
    localStorage.setItem('theme', theme);
  }, [theme]);

  const toggleTheme = () => {
    setTheme((prevTheme) => (prevTheme === 'light' ? 'dark' : 'light'));
  };
  return (
    <NavLink onClick={toggleTheme}>{theme === 'dark' ? '☀️' : '🌙'}</NavLink>
  )
}

export default ThemeSwitcher;