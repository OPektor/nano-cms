import React, { useEffect, useState } from 'react'
import { NavDropdown } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';

interface LocaleSwitcherParams {
    locales: string[];
    updateLocale: (locale: string) => void;
}

const LocaleSwitcher: React.FC<LocaleSwitcherParams> = ({ locales, updateLocale }) => {
    const navigate = useNavigate();

    const [locale, setLocale] = useState<string>("cs");
    useEffect(() => {
        let l = localStorage.getItem("locale");
        if (!l) {
            let language = (navigator.language || navigator.languages[0]).split('-').at(0);
            if (!language || locales.findIndex(x => x === language) === -1) {
                language = locales[0];
            }
            localeChange(language, true);
        } else {
            localeChange(l, true);
        }
    }, [locales, updateLocale]);

    const localeChange = (l: string, force: boolean) => {
        if (locale !== l || force) {
            setLocale(l);
            updateLocale(l);
            localStorage.setItem("locale", l);
            if (!force) {
                navigate("/");
            }
        }
    };

    return (
        <>
            {
                locales?.length > 0 ? <NavDropdown title={locale}>
                    {locales?.map((l, i) =>
                        <NavDropdown.Item key={i} onClick={() => localeChange(l, false)}>
                            {l}
                        </NavDropdown.Item>
                    )}
                </NavDropdown> : ""
            }
        </>
    )
}

export default LocaleSwitcher;