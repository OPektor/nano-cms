import { Link } from "react-router-dom";

// Custom link renderer to use react-router-dom's Link
interface LinkRendererProps {
    href?: string;
    children: React.ReactNode;
}

const LinkRenderer: React.FC<LinkRendererProps> = ({ href, children }) => {
    if (href && href.replaceAll('.', '').startsWith('/')) {
        // Use Link for internal links
        return <Link to={href}>{children}</Link>;
    }
    // Default to an anchor tag for external links
    return <a target="_blank" rel="noreferrer noopener" href={href}>{children}</a>;
};

export default LinkRenderer;