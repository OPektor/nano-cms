export interface Index {
    WebName: string;
    Pages: Page[];
    Sections: Section[];
    Links: Link[];
    MenuImage: string;
    Locales: string[];
    DefaultPage: string;
}

export interface Page {
    Name: string;
    Path: string;
    Locale: string;
}

export interface Section {
    Names: LocaleName[];
    Path: string;
    Pages: Page[];
    Sections: Section[];
    Links: Link[];
}

export interface Link {
    Title: string;
    Url: string;
    Locale: string;
}

export interface LocaleName {
    Name: string;
    Locale: string;
}