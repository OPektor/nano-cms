import { Index } from "./contracts/Index";
import config from "../config.json"

export class ApiClient {
    public static async getIndex(): Promise<Index> {
        const response = await fetch(`${config.Server_URL}/index`);
        return await response.json();
    }

    public static async loadPage(path: string): Promise<string> {
        const response = await fetch(`${config.Server_URL}/page`, {
            method: "POST",
            headers: {
                'Accept': 'text/plain',
                'Content-Type': 'text/plain',
                "locale": this.getLocale()
            },
            body: path
        });
        if (!response.ok) {
            return "";
        }
        return await response.text();
    }

    private static getLocale(): string {
        let locale = localStorage.getItem("locale");

        if (!locale) {
            const language = (navigator.language || navigator.languages[0]).split('-').at(-1);
            if (language) {
                locale = language;
            } else {
                locale = "en";
            }
        }

        return locale;
    }
}