# Nano CMS

## Project philosophy

This application should serve as a simple way how to create and maintain static web pages using Markdown language. Application will create whole web structure based on files and folder structure inside your content folder. This is not a CMS in the true sense of the word but it can help users who are able to edit Markdown files on the server.

Architecture of this application is split to Golang backend (runs on server) and React frontend (runs in the browser). 

## How to run the application

To run this application on your local machine you will need `Golang 1.22.2` or newer and `Node.js`. 

1. Install all node modules in the frontend project (`react-nanocms`) with command `npm install`.

2. Configure backend and frontend using its `config.json` files.

3. Create default page (markdown) in the `ContentPath` folder and put its name without extension and without localization into `DefaultPage` property. (For example: from `home.cs.md` to `/home`).

4. Launch the Golang backend application with command `go run main.go` inside the folder `/GO-NanoCMS`.

5. Launch the React frontend application with command `npm run start` inside the folder `react-nanocms`.

### Backend config

In the backend part of this project there is `GO-NanoCMS/config.json` file. In this file there are a lot of settings regarding whole application.

```json
{
    "ContentPath": "../pages",
    "IndexFile": "./index.json",
    "LocalPort": 5559,
    "FrontEndUrl": "http://localhost:3000",
    "BackEndUrl": "http://localhost:5559",
    "WebName": "My web",
    "DefaultPage": "/home",
    "MenuImage": "menu_image.png",
    "Locales": [
        "en",
        "cs"
    ]
}
```

1. **ContentPath**
    
    This is relative path to the folder where content of your pages are stored (markdown files, config JSONs, sub-folders and images).

2. **IndexFile**

    This is path where the index.json file should be generated based on your content folder.

3. **LocalPort**

    This is an HTTP port that backend will use to run on.

4. **FrontEndUrl**

    This is an URL where the frontend will be served. It's used for creation of links between pages of your web.

5. **BackEndUrl**

    This is an URL where the backend will be served. It's used for creating links to images.

6. **WebName**

    This name is shown in the top navbar of your web.

7. **DefaultPage**

    This is route to the default (index or home) page that will be served at `/` route.

8. **MenuImage**

    This image will be placed nex to the web name in the top navbar.

9. **Locales**

    This is list of localizations you want to support at your web.

### Frontend config

In the frontend part of this project there is `react-nanocms/src/config.json` file. In this file is just an URL to your backend instance.

```json
{
    "Server_URL": "http://localhost:5559"
}
```

## How to create a page

The page is a simple Markdown file with extension `.md`. I you wan to use some page just in a single language you can set it in the name before the extension (for example `home.en.md` - this page has `en` as language). If you want to have one page in two languages you can create two files with same name and different language (for example `home.en.md` and `home.cs.md`).

## Section settings

A Section is created automatically when you create a sub-folder inside your content folder. You can set name for this section for multiple languages and add some external links to the section by creating `section.json` config file.

If you don't set `Locale` property to the link, it will be displayed for all the languages.

```json
{
    "Names": [
        {
            "Name": "Název sekce",
            "Locale": "cs"
        },
        {
            "Name": "Section name",
            "Locale": "en"
        }
    ],
    "Links": [
        {
            "Title": "Odkaz v češtině",
            "Url": "https://about.gitlab.com/",
            "Locale": "cs"
        },
        {
            "Title": "Link in english",
            "Url": "https://about.gitlab.com/",
            "Locale": "en"
        }
    ]
}
```

## How to add an image

If you wan to add an image you can use a standard Markdown notation `![alt](url)`. You should put the image into the same folder as the page and then use relative path to the image as the url. 

## How to add link to another page

If you wan to add ling from one page to the another you can use standard Markdown notation for link `[text](url)` and use relative path to the another page as the url.